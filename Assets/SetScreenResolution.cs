﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetScreenResolution : MonoBehaviour
{
    [SerializeField]
    int width, height;

    private void Awake()
    {
        //Set screen size for Standalone
#if UNITY_STANDALONE
        Screen.SetResolution(width, height, false);
        Screen.fullScreen = false;
#endif
    }
}
