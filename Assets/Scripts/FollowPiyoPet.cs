﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPiyoPet : MonoBehaviour
{
    [SerializeField] Transform PiyoPet;
    [SerializeField] float offSet;
    // Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.gameObject.transform.position = new Vector3 (PiyoPet.position.x, offSet, PiyoPet.transform.position.z);
	}
}
