﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomCamera : MonoBehaviour
{
    [SerializeField] Camera theCamera;
    [SerializeField] GameObject rootObject;
    [SerializeField] Transform piyoPetZoomLoaction;
    [SerializeField] float orginalOrthoSize;
    [SerializeField] float amountToShrinkPerTick;
    [SerializeField] float targetShrinkAmount;
    [SerializeField] float timePerTick;
    [SerializeField] float cameraOrginalY;
    private Transform orginalLocation;
    public static bool startShrinking;


    // Update is called once per frame
    void Update ()
    {
		if (startShrinking == true && theCamera.orthographicSize >= 5)
        {
            StartCoroutine("zoomIn");
        }
        else if (startShrinking == false && theCamera.orthographicSize < orginalOrthoSize)
        {
            StopCoroutine("zoomIn");
            StartCoroutine("zoomOut");
        }
	}

    IEnumerator zoomIn()
    {
        orginalLocation = rootObject.transform;
        rootObject.transform.position = new Vector3(piyoPetZoomLoaction.position.x, piyoPetZoomLoaction.position.y, -108);

        while (theCamera.orthographicSize >= targetShrinkAmount)
        {
            theCamera.orthographicSize -= amountToShrinkPerTick;
            yield return new WaitForSeconds(timePerTick);
        }
    }

    IEnumerator zoomOut()
    {
        while (theCamera.orthographicSize <= orginalOrthoSize)
        {
            theCamera.orthographicSize += amountToShrinkPerTick;

            yield return new WaitForSeconds(timePerTick);
        }

        //rootObject.transform.position = new Vector3(orginalLocation.position.x, cameraOrginalY, 0);
    }



}
