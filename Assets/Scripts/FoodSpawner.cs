﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
   [SerializeField] GameObject foodPrefab;
    [SerializeField] GameObject liveFood;
	// Use this for initialization
	void Awake () 
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (liveFood == null)
        {
            liveFood = Instantiate(foodPrefab,this.transform);
            
        }
	}
}
