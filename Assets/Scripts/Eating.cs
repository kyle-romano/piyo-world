﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class Eating : MonoBehaviour
{
    IEnumerator eatingRoutine;
    GameObject heldFood;
    public bool foodHeldOverPiyo;
    private bool biting;
    [SerializeField] float timeBetweenBites;
    [SerializeField] float amountToReduceSizeBy;
    [SerializeField] float minSize;
    [SerializeField] ParticleSystem constantParticles;
    [SerializeField] ParticleSystem burstParticles;
    [SerializeField] Pathfinding.AIPath AipathScript;
    [SerializeField] string foodPrefernce;
    [SerializeField] string heldFoodType;
    [SerializeField] int postionOfChild;
    public SkeletonAnimation spineAnim;
    Spine.Event bite;
    public bool isHappy;
    Spine.AnimationState state;
    public bool piyoEating;
    TrackEntry trackEntry;

    [SpineEvent] public string biteEventName;


    // Use this for initialization
    void Awake()
    {
        biteEventName = "bite";
        eatingRoutine = EatingAnimation();
        spineAnim = GetComponentInChildren<SkeletonAnimation>();
    }

    private void Start()
    {
        spineAnim.state.Event += HandleEvent;
        spineAnim.state.Start += delegate (Spine.TrackEntry entry) { };
        spineAnim.state.End += delegate { };
    }

    void HandleEvent(Spine.TrackEntry entry, Spine.Event e)
    {
        if (e.data.name == biteEventName && piyoEating == false)
        {
                StartCoroutine("EatingAnimation");
        }
    }

    IEnumerator EatingAnimation()
    {

        while (foodHeldOverPiyo == true)
        {
            piyoEating = true;
            heldFood.transform.localScale = new Vector3(heldFood.transform.localScale.x - amountToReduceSizeBy, heldFood.transform.localScale.y - amountToReduceSizeBy, 1);

            burstParticles.Play();

            if (heldFood.transform.localScale.x <= minSize)
            {
                AipathScript.canMove = true;
                DragCamera.waitingForMouseRelease = true;
                burstParticles.Play(); 
                checkFoodPrefernce();
                Destroy(heldFood.transform.parent.gameObject);
                DragCamera.pickedUpObject = false;
                foodHeldOverPiyo = false;
                piyoEating = false;
            }
           yield return new WaitForSeconds(timeBetweenBites);
        }
    }

    void checkFoodPrefernce()
    {
        if(heldFood != null)
        {
            heldFoodType = heldFood.GetComponent<SpriteRenderer>().sprite.name;
        }

        if (foodPrefernce == heldFoodType)
        {
            
            isHappy = true;
            //spineAnim.AnimationName = ("happy");
            //Debug.Log(1);
            StartCoroutine (HappyTime());
        }
    }

    IEnumerator HappyTime()
    {
        trackEntry = spineAnim.AnimationState.SetAnimation(1, "happy", false);
        yield return new WaitForSpineAnimationComplete(trackEntry);

        isHappy = false;
        if (trackEntry.IsComplete)
        {
            spineAnim.AnimationState.ClearTrack(1);
            //spineAnim.AnimationState.SetAnimation(0,"idle",true);
            yield return null;
        }
        //spineAnim.AnimationState.SetEmptyAnimation(1,0);
        // trackEntry = spineAnim.AnimationState.SetAnimation(0, "idle", true);
    
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Item" && foodHeldOverPiyo == false && DragCamera.pickedUpObject == true)
        {
            AipathScript.canMove = false;
            heldFood = collision.transform.GetChild(0).gameObject;
            burstParticles = collision.transform.GetChild(postionOfChild).GetComponent<ParticleSystem>();
            foodHeldOverPiyo = true;
            spineAnim.AnimationName = "eating";
            ZoomCamera.startShrinking = true;
            StartCoroutine("EatingAnimation");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Item" && DragCamera.pickedUpObject == true)
        {
            AipathScript.canMove = true;
            if (heldFood != null && burstParticles != null)
            {
                heldFood.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
                //constantParticles.Stop();
                burstParticles.Stop();
            }

            foodHeldOverPiyo = false;
            StopCoroutine("EatingAnimation");
            ZoomCamera.startShrinking = false;
            spineAnim.AnimationName = "idle";

        }
    }
}
