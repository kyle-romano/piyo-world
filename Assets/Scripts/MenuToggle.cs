﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuToggle : MonoBehaviour 
{

    //I'm probably supposed to use RectTransform for UI elements, but at the time I'm writing this, I'm not sure how to use it, and I
    //  can't focus enough to figure it out myself. Sorry, but you'll have to deal with my terrible alternative. v>v

    public float yActive, yInactive, transitionTime;
    float timer;

    RectTransform rectTransform;

    Vector3 activePosition, inactivePosition;

   public bool windowOpen;
    
    private void Start() {
        inactivePosition = new Vector3(transform.position.x, yInactive, transform.position.z);

        activePosition = new Vector3(transform.position.x, yActive, transform.position.z);
    }
    
    public void Toggle() {
        /*if (!windowOpen) {
            transform.position = activePosition;
        } else {
            transform.position = inactivePosition;
        }*/
        windowOpen = !windowOpen;
    }

    private void Update()
    {
        if (windowOpen && transform.position != activePosition && timer < transitionTime)
        {
            inactivePosition = new Vector3(transform.parent.position.x, yInactive, transform.parent.position.z);
            activePosition = new Vector3(transform.parent.position.x, yActive, transform.parent.position.z);

            timer += Time.deltaTime;
            transform.position = Vector3.Lerp(inactivePosition, activePosition, (timer/transitionTime));

        }
        else if (!windowOpen && transform.position != inactivePosition && timer > 0)
        {
            inactivePosition = new Vector3(transform.parent.position.x, yInactive, transform.parent.position.z);
            activePosition = new Vector3(transform.parent.position.x, yActive, transform.parent.position.z);

            timer -= Time.deltaTime;
            transform.position = Vector3.Lerp(activePosition, inactivePosition, ((transitionTime - timer) / transitionTime));
        }
    }

}
