﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    RectTransform rectTransform;
    public static GameObject item;
    Vector3 startPosition;
    Transform startTransform;
    public Transform heldItemObject;
    public GameObject itemPrefab;
    [SerializeField] Rigidbody2D dragebleRigidbody;

    public void OnBeginDrag(PointerEventData eventData)
    {
        item = gameObject;
        startPosition = transform.position;
        startTransform = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        //transform.SetParent(heldItemObject);
        rectTransform = GetComponent<RectTransform>();

        dragebleRigidbody.gravityScale = 0;
        DragCamera.pickedUpObject = true;
        TurnOffPettingBoc.heldItemIsFood = true;
        //dragebleRigidbody.inertia = 0;
        //dragebleRigidbody.velocity = new Vector2 (0,0);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
        dragebleRigidbody.inertia = 0;
        dragebleRigidbody.velocity = new Vector2(0, 0);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        item = null;
        dragebleRigidbody.gravityScale = 1;
        DragCamera.pickedUpObject = false;
        TurnOffPettingBoc.heldItemIsFood = false;
        if (transform.parent == heldItemObject)
        {
            //transform.SetParent(startTransform);
        }
        //transform.position = transform.parent.position;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }
}
