﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffPettingBoc : MonoBehaviour 
{
   [SerializeField] Collider2D pettingBox;
    [SerializeField] Collider2D eatingBox; 
    
    public static bool heldItemIsFood;
    public MoveToClickedPoint interactionScript;
    public Eating eatScript;

    private void Start()
    {
        interactionScript = GetComponent<MoveToClickedPoint>();
        eatScript = GetComponent<Eating>();
    }


    // Update is called once per frame
    void Update ()
    {
        if (heldItemIsFood == true)
        {
            pettingBox.enabled = false;
            eatingBox.enabled = true;
        }
        else if(heldItemIsFood == false && pettingBox.enabled == false)
        {
            pettingBox.enabled = true;
            eatingBox.enabled = false;
        }
        
       
        
    }

    private void OnMouseDrag()
    {
        if (DragCamera.pickedUpObject == false && Mathf.Abs(Camera.main.ScreenToWorldPoint(Input.mousePosition).x - this.gameObject.transform.position.x) <= 0.5f)
        {
            if (Input.GetMouseButton(0))
            {
                Debug.Log("beingpet");
                DragCamera.waitingForMouseRelease = true;
                interactionScript.spineAnim.AnimationName = ("getting petted (rub)");
            }
        }
        if (Input.GetMouseButtonUp(0) && interactionScript.isWalking == false && eatScript.isHappy == false)
        {

            DragCamera.waitingForMouseRelease = false;
        }

    }

    private void OnMouseUp()
    {
        interactionScript.spineAnim.AnimationName = ("idle");
    }


}
