﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragOutOfInvField : MonoBehaviour, IPointerEnterHandler
{
    
    public MenuToggle menu;

    public GameObject canvasSlotOne;
    public GameObject canvasSlotTwo;

    [SerializeField] GameObject ItemOne;
    [SerializeField] GameObject ItemTwo;


    public void OnPointerEnter(PointerEventData eventData)
    {

        if (DragHandler.item)
        {
            GameObject itemInInventory = DragHandler.item; 
            GameObject pulledItem = Instantiate(ItemOne, canvasSlotOne.transform.position, new Quaternion(0, 0, 0, 0)) as GameObject;
            //pulledItem.transform.parent = canvasSlotOne.transform;

           // DragHandler.item = null;
            menu.Toggle();
        }
    }
}
// GameObject pulledItem = Instantiate(itemInInventory.GetComponent<DragHandler>().itemPrefab, Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)), new Quaternion(0,0,0,0));
//pulledItem.GetComponent<DragObject>().SpawnInHand();