﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class SpawnFoodAtCamera : MonoBehaviour
{
    [SerializeField] GameObject prefabOne;
    [SerializeField] GameObject cameraMain;
    [SerializeField] MenuToggle menuToggleHolder;
  

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Game Object clicked");
        //make the item 
        Instantiate(prefabOne, cameraMain.transform.position, new Quaternion(0, 0, 0, 0));

        //close the menu
        menuToggleHolder.windowOpen = false;
    }

    private void OnMouseDown()
    {
        Debug.Log("Game Object clicked");
        //make the item 
        Instantiate(prefabOne, cameraMain.transform.position, new Quaternion(0, 0, 0, 0));

        //close the menu
        menuToggleHolder.windowOpen = false;
    }
}
