﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    Vector3 screenPoint;
    Vector3 offset;
    Vector3 followPoint;
    Vector3 currentPostion;
    [SerializeField] Rigidbody2D dragebleRigidbody;


    private void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(transform.position);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        dragebleRigidbody.gravityScale = 0;
        DragCamera.pickedUpObject = true;
        TurnOffPettingBoc.heldItemIsFood = true;
    }

    private void OnMouseDrag()
    {
        followPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        currentPostion = Camera.main.ScreenToWorldPoint(followPoint);
        transform.position = currentPostion;
    }

    private void OnMouseUp()
    {
        dragebleRigidbody.gravityScale = 1;
        DragCamera.pickedUpObject = false;
        TurnOffPettingBoc.heldItemIsFood = false;
    }

    public void SpawnInHand()
    {
        screenPoint = Camera.main.WorldToScreenPoint(transform.position);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        //DragCamera.pickedUpObject = true;
        //dragebleRigidbody.gravityScale = 0;
        //followPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        //currentPostion = Camera.main.ScreenToWorldPoint(followPoint);
        //transform.position = currentPostion;
    }
}
