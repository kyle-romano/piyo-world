﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dressup : MonoBehaviour
{

    public SpriteRenderer hatSprite, bodySprite, outfitSprite;
    
    static Sprite hat, body, outfit;
    static string hatName, bodyName, outfitName;



    private void Awake()
    {
        hatSprite.sprite = hat;
        bodySprite.sprite = body;
        outfitSprite.sprite = outfit;
    }


    public void PutOnNewHat(Sprite newHat)
    {
        if (hat == newHat)
        {
            hat = null;
        } else
        {
            hat = newHat;
        }
        hatSprite.sprite = hat;
    }

    public void PutOnNewBody(Sprite newBody)
    {
        if (body == newBody)
        {
            body = null;
            
        }
        else
        {
            body = newBody;
        }
        bodySprite.sprite = body;
    }

    public void PutOnNewOutfit(Sprite newOutfit)
    {
        if (outfit == newOutfit)
        {
            outfit = null;
        }
        else
        {
            outfit = newOutfit;
        }
        outfitSprite.sprite = outfit;
    }

    public void ResetClothes()
    {
        hat = null;
        body = null;
        outfit = null;
        hatSprite.sprite = hat;
        bodySprite.sprite = body;
        outfitSprite.sprite = outfit;
    }
}
