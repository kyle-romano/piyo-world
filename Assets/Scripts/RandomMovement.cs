﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovement : MonoBehaviour
{ 
    [SerializeField]Pathfinding.AIDestinationSetter AidestinationScript;
    [SerializeField]GameObject aiDestination;
    [SerializeField]Transform destinationTransform;
    [SerializeField] float leftLimit;
    [SerializeField] float rightLimit;
    [SerializeField] float waitTime;
    [SerializeField] SpriteRenderer SpriteRenderer;
    [SerializeField] Pathfinding.AIPath AIPathScript;

    bool playerInteraction;
    float moveAmount;
    IEnumerator startTheRandomization;

	// Use this for initialization
	void Awake()
    {
        startTheRandomization = Setdestination();
        playerInteraction = false;
        StartCoroutine(startTheRandomization);
        aiDestination.transform.position = new Vector3(0, 0, 0);
        AidestinationScript.target = destinationTransform;
    }
	
    void PlayAnimation()
    {
    }

    IEnumerator Setdestination()
    {
        while (playerInteraction != true)
        {
            if (moveAmount <= transform.position.x)
            {
                SpriteRenderer.flipX = true;
            }
            if (moveAmount >= transform.position.x)
            {
                SpriteRenderer.flipX = false;
            }
            moveAmount = Random.Range(leftLimit, rightLimit);
            
            destinationTransform.position = new Vector3(moveAmount, -4.2f, 0);

            yield return new WaitForSeconds(waitTime);
        }
        yield return null;
    }

    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Item" && playerInteraction == false)
        {
             
            playerInteraction = true;
            AIPathScript.canMove = false;
            
            //StopCoroutine(startTheRandomization);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Item" && playerInteraction == true)
        {
            playerInteraction = false;
            AIPathScript.canMove = true;
            //  StartCoroutine(startTheRandomization);
        }
    }
    
}
