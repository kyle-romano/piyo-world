﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

    public int targetSceneIndex;

    public void ChangeToTargetScene()
    {
        SceneManager.LoadScene(targetSceneIndex);
    }
}
