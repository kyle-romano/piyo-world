﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchTabs : MonoBehaviour {

    public List<GameObject> tabContents;

    public void OpenTab(int tabIndex)
    {
        foreach (GameObject g in tabContents)
        {
            g.SetActive(false);
        }

        tabContents[tabIndex].SetActive(true);
    }
}
