﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class MoveToClickedPoint : MonoBehaviour
{
    [SerializeField] GameObject destinationGameObject;
    [SerializeField] Pathfinding.AIDestinationSetter aiDestinationSetterScript;
    [SerializeField] Pathfinding.AIPath AIPathScript;
    Vector3 mouseWorldPoint;
    [SerializeField] SpriteRenderer SpriteRenderer;
    public SkeletonAnimation spineAnim;
    public bool isWalking;
    TrackEntry eatAnticipationTrackEntry;


    // Use this for initialization
    void Awake ()
    {
        spineAnim = GetComponentInChildren<SkeletonAnimation>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(0) == true)
        {
            if (DragCamera.pickedUpObject == false)
            {
                mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                destinationGameObject.transform.position = new Vector3(mouseWorldPoint.x, mouseWorldPoint.y, 0);
                if (destinationGameObject.transform.position.x < this.gameObject.transform.position.x)
                {
                    //SpriteRenderer.flipX = true; 
                    spineAnim.Skeleton.FlipX = false;
                }
                else
                {
                    spineAnim.Skeleton.FlipX = true;
                    // SpriteRenderer.flipX = false;
                }
                aiDestinationSetterScript.target = destinationGameObject.transform;


                spineAnim.AnimationName = "walk";
                isWalking = true;
            }
            
        }

        if (Mathf.Abs(destinationGameObject.transform.position.x - this.gameObject.transform.position.x) <= 0.5f && isWalking == true)
        {

            if (GetComponent<Eating>().foodHeldOverPiyo == false && DragCamera.pickedUpObject == false)
            {
               
                spineAnim.AnimationName = "idle";
                isWalking = false;
            }
        }
        if (DragCamera.pickedUpObject == true)
        {
          
            if (GetComponent<Eating>().foodHeldOverPiyo == false)
            {
                spineAnim.AnimationName = "eat anticipation";

               // eatAnticipationTrackEntry = spineAnim.AnimationState.SetAnimation(1, "eat anticipation", true);

                // BELOW doesn't work as intended

                // Debug.Log(eatAnticipationTrackEntry);

                //if (eatAnticipationTrackEntry.trackTime == eatAnticipationTrackEntry.trackEnd)
                //{
                //    eatAnticipationTrackEntry.trackTime = 9f / 30f;
                //}
            }

        }
        //Debug.Log(Mathf.Abs(destinationGameObject.transform.position.x - this.gameObject.transform.position.x));
    }

  
      
   

}
