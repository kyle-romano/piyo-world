﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragCamera : MonoBehaviour
{
    [SerializeField] float limitsRight;
    [SerializeField] float limitsLeft;
    [SerializeField] float sensistivty;
    private Vector3 lastPosition;
    Vector3 delta;
    public static bool waitingForMouseRelease;
    public static bool pickedUpObject;

    private void Awake()
    {
        pickedUpObject = false;
    }

    private void Update()
    {
        if (pickedUpObject == false && PettingBehaviour.pettingEngaged == false && waitingForMouseRelease == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                lastPosition = Input.mousePosition;
            }

            if (Input.GetMouseButton(0))
            {
                delta = Input.mousePosition - lastPosition;
                transform.Translate(-(delta.x * sensistivty), 0, 0);
                lastPosition = Input.mousePosition;
            }
        }


        if (transform.position.x > limitsRight)
        {
            transform.position = new Vector3(limitsRight, 0, transform.position.z);
        }
        if (transform.position.x < limitsLeft)
        {
            transform.position = new Vector3(limitsLeft, 0, transform.position.z);
        }

        if (Input.GetMouseButtonUp(0) && waitingForMouseRelease == true)
        {
            waitingForMouseRelease = false;
        }
    }

}

