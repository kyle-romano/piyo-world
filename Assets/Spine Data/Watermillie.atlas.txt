
Watermillie.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
blush
  rotate: false
  xy: 2, 110
  size: 407, 122
  orig: 407, 122
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 2, 234
  size: 769, 1030
  orig: 769, 1030
  offset: 0, 0
  index: -1
left_arm
  rotate: false
  xy: 411, 127
  size: 120, 105
  orig: 120, 105
  offset: 0, 0
  index: -1
left_eye
  rotate: false
  xy: 1034, 332
  size: 64, 126
  orig: 64, 126
  offset: 0, 0
  index: -1
left_eye_CLOSED
  rotate: false
  xy: 1034, 460
  size: 116, 160
  orig: 116, 160
  offset: 0, 0
  index: -1
left_foot
  rotate: false
  xy: 1411, 905
  size: 67, 98
  orig: 67, 98
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 533, 139
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
mouth_CLOSED
  rotate: false
  xy: 1152, 494
  size: 123, 126
  orig: 123, 126
  offset: 0, 0
  index: -1
ribbon
  rotate: false
  xy: 1034, 622
  size: 264, 381
  orig: 264, 381
  offset: 0, 0
  index: -1
right_arm
  rotate: false
  xy: 2, 2
  size: 148, 106
  orig: 148, 106
  offset: 0, 0
  index: -1
right_eye
  rotate: false
  xy: 1300, 722
  size: 59, 117
  orig: 59, 117
  offset: 0, 0
  index: -1
right_eye_CLOSED
  rotate: false
  xy: 1300, 841
  size: 109, 162
  orig: 109, 162
  offset: 0, 0
  index: -1
right_foot
  rotate: false
  xy: 152, 15
  size: 67, 93
  orig: 67, 93
  offset: 0, 0
  index: -1
