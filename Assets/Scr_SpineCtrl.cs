﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
public class Scr_SpineCtrl : MonoBehaviour {
    public SkeletonAnimation spineAnim;
    Vector3 destinationWorldPoint;
    Vector3 mouseWorldPoint;
	// Use this for initialization
	void Start () {
        spineAnim = GetComponent<SkeletonAnimation>();
	}

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButton(0))
        {

            spineAnim.AnimationName = "walk";
            
        }
        if (Input.GetMouseButtonUp(0))
        {
            spineAnim.AnimationName = "idle";
        }
	}
}
